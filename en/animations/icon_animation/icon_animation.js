function Icon_animation(resources)
{
	Icon_animation.resources = resources;
}
Icon_animation.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(900, 400, Phaser.CANVAS, 'icon_animation', { preload: this.preload, create: this.create, update: this.update, render:
		this.render,parent:this },null,null,false);
	},

	preload: function()
	{

		this.game.scale.maxWidth = 900;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		//this.game.load.image('placeholder',Icon_animation.resources.placeholder);
		this.game.load.image('icon_1',Icon_animation.resources.icon_1);
        this.game.load.image('icon_2',Icon_animation.resources.icon_2);
        this.game.load.image('icon_3',Icon_animation.resources.icon_3);
		//debugger

		this.game.created = false;




    	this.game.stage.backgroundColor = '#ffffff';
    	//0f2747

	},

	create: function(evt)
	{

		if(this.game.created === false)
		{

			this.parent.laptop = this.game.make.sprite(0,0,'icon_1');
			this.parent.laptop.anchor.set(0.5,0.5);
            this.parent.game.stage.backgroundColor = '#ffffff';
			this.game.created  = true;
	    	this.parent.buildAnimation();
	    }
	},

	buildAnimation: function()
	{
		var style = Icon_animation.resources.textStyle_1;
        this.icon_1= this.game.add.sprite(this.game.world.centerX-250,this.game.world.centerY-50,'icon_1');
        this.icon_1.anchor.set(0.5,0.5);
        //
        this.icon_2= this.game.add.sprite(this.game.world.centerX,this.game.world.centerY-50,'icon_2');
        this.icon_2.anchor.set(0.5,0.5);
        //
        this.icon_3= this.game.add.sprite(this.game.world.centerX+250,this.game.world.centerY-50,'icon_3');
        this.icon_3.anchor.set(0.5,0.5);
        //
        var text_1 = this.game.add.text(this.game.world.centerX-260,this.game.world.centerY+70, Icon_animation.resources.text_1, style);
        text_1.anchor.set(0.5,0.5);
        //
        var text_2 = this.game.add.text(this.game.world.centerX+5,this.game.world.centerY+70, Icon_animation.resources.text_2, style);
        text_2.anchor.set(0.5,0.5);
        //
        var text_3 = this.game.add.text(this.game.world.centerX+250,this.game.world.centerY+70, Icon_animation.resources.text_3, style);
        text_3.anchor.set(0.5,0.5);

        //animations

        this.icon_1_An = this.game.add.tween(this.icon_1).to( { alpha:1,y:this.icon_1.y},700,Phaser.Easing.Quadratic.Out);
        this.icon_1.y-=50;
        this.icon_1.alpha = 0;
        //
        this.icon_2_An = this.game.add.tween(this.icon_2).to( { alpha:1,y:this.icon_2.y},700,Phaser.Easing.Quadratic.Out);
        this.icon_2.y-=50;
        this.icon_2.alpha = 0;
        //
        //
        this.icon_3_An = this.game.add.tween(this.icon_3).to( { alpha:1,y:this.icon_3.y},700,Phaser.Easing.Quadratic.Out);
        this.icon_3.y-=50;
        this.icon_3.alpha = 0;


        this.text_1_An = this.game.add.tween(text_1).to( { alpha:1,y:text_1.y},700,Phaser.Easing.Quadratic.Out);
        text_1.y+=50;
        text_1.alpha = 0;
        //
        this.text_2_An = this.game.add.tween(text_2).to( { alpha:1,y:text_2.y},700,Phaser.Easing.Quadratic.Out);
        text_2.y+=50;
        text_2.alpha = 0;
        //
        this.text_3_An = this.game.add.tween(text_3).to( { alpha:1,y:text_3.y},700,Phaser.Easing.Quadratic.Out);
        text_3.y+=50;
        text_3.alpha = 0;




        //start animation


        this.icon_3_An.chain(this.text_3_An,this.icon_2_An,this.text_2_An,this.icon_1_An,this.text_1_An);
        this.icon_3_An.start();

    },
	inview: function()
	{


	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
//course/en/animations/nielsen_audience/
